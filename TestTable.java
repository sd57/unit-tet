/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.oxoop.Table;
import com.mycompany.oxoop.player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Acer
 */
public class TestTable {
    
    public TestTable() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX(){
        player x = new player('X');
         player o = new player('O');
         Table table = new Table(x,o);
         table.setRowCol(0, 0);
         table.setRowCol(0, 1);
         table.setRowCol(0, 2);
         table.checkWin();
         assertEquals(true,table.isFinsish());
         assertEquals(x, table.getWinner());
    }
    @Test
      public void testRow2ByX(){
        player x = new player('x');
         player o = new player('o');
         Table table = new Table(x,o);
         table.setRowCol(1, 0);
         table.setRowCol(1, 1);
         table.setRowCol(1, 2);
         table.checkWin();
         assertEquals(true,table.isFinsish());
         assertEquals(x,table.getWinner());
     }
      @Test
     public void testRow3ByX(){
        player x = new player('x');
         player o = new player('o');
         Table table = new Table(x,o);
         table.setRowCol(2, 0);
         table.setRowCol(2, 1);
         table.setRowCol(2, 2);
         table.checkWin();
         assertEquals(true,table.isFinsish());
         assertEquals(x, table.getWinner());
     }
     @Test
    public void testRow1ByO(){
        player x = new player('X');
         player o = new player('O');
         Table table = new Table(x,o);
         table.setRowCol(0, 0);
         table.setRowCol(1, 0);
         table.setRowCol(2, 0);
         table.checkWin();
         assertEquals(true,table.isFinsish());
         assertEquals(x, table.getWinner());
    }
    @Test
      public void testRow2ByO(){
        player x = new player('x');
         player o = new player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 1);
         table.setRowCol(1, 1);
         table.setRowCol(2, 1);
         table.checkWin();
         assertEquals(true,table.isFinsish());
         assertEquals(x,table.getWinner());
     }
      @Test
     public void testRow3ByO(){
        player x = new player('x');
         player o = new player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 2);
         table.setRowCol(1, 2);
         table.setRowCol(2, 2);
         table.checkWin();
         assertEquals(true,table.isFinsish());
         assertEquals(x, table.getWinner());
     }
      @Test
     public void testnallByX(){
        player x = new player('x');
         player o = new player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 0);
         table.setRowCol(1, 1);
         table.setRowCol(2, 2);
         table.checkWin();
         assertEquals(true,table.isFinsish());
         assertEquals(x, table.getWinner());
     }
     @Test
     public void testnallByO(){
        player x = new player('x');
         player o = new player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 0);
         table.setRowCol(1, 1);
         table.setRowCol(2, 2);
         table.checkWin();
         assertEquals(true,table.isFinsish());
         assertEquals(x, table.getWinner());
     }
      @Test
     public void testdiagonalrightByX(){
        player x = new player('x');
         player o = new player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 2);
         table.setRowCol(1, 1);
         table.setRowCol(2, 0);
         table.checkWin();
         assertEquals(true,table.isFinsish());
         assertEquals(x, table.getWinner());
     }
     @Test
     public void testdiagonalrightByO(){
        player x = new player('x');
         player o = new player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 2);
         table.setRowCol(1, 1);
         table.setRowCol(2, 0);
         table.checkWin();
         assertEquals(true,table.isFinsish());
         assertEquals(x, table.getWinner());
     }
     @Test
     public void testRowColumnIsEmpty(){
        player x = new player('x');
         player o = new player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 0);
         assertEquals(false,table.setRowCol(0, 0));
     }




      
}
